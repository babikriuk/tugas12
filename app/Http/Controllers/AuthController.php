<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function daftar()
    {
        return view('register');    
    }

    public function dashboard(Request $req)
    {
        $firstName = $req['firstName'];
        $lastName = $req['lastName'];
        //echo $firstName . " - " . $lastName;
        return view('dashboard', ['firstName' => $firstName, 'lastName'=>$lastName]);
    }
}
