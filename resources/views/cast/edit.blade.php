@extends('layout.master')
@section('judul')
<H1>Update Data Cast</H1>
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
       @method('PUT')
        <div class="mb-3">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="{{$cast->nama}} ">
            @error('nama')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" placeholder="Umur" value="{{$cast->umur}}">
            @error('umur')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="bio">Bio</label>
            <textarea class="form-control" name="bio" id="bio" rows="6">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary mb-3">Save</button>
        </div>
    </form>
@endsection    
    
