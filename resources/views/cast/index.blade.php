@extends('layout.master')
@section('judul')
<H1>Cast</H1>
@endsection

@section('content')
<a href="/cast/create">Create New Cast</a>
        @foreach ($casts as $item)
        
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                <h3 class="card-title">{{$item->nama}}</h5><br>
                <h5 class="card-title">{{$item->umur}}</h5>
                <p class="card-text">{{$item->bio}}</p>
                <a href="/cast/{{$item->id}}" class="btn btn-info">Show</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
                </div>
            </div>
        @endforeach
    
   
@endsection    
    
