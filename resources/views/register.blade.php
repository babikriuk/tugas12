@extends('layout.master')
@section('judul')
<H1>Buat Account Baru</H1>
<h2>Sign Up Form</h2>
@endsection
@section('content')
<form action="/register" method="post">
    @csrf
    <label>First Name</label><br>
    <input type="text" name="firstName"><br><br>
    <label>Last Name</label><br>
    <input type="text" name="lastName"><br><br>
    <label>Gender </label><br>
    <input type="radio" name="gender" id="male">Male<br>
    <input type="radio" name="gender" id="female">Female<br>
    <label>Nationality : </label><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option>
    </select><br>  
    <label>Language Spoken : </label><br>  
    <input type="checkbox" name="language" id="english">English<br>
    <input type="checkbox" name="language" id="indonesian">Indonesian<br>
    <input type="checkbox" name="language" id="javanese">Javanese<br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="SignUp">
</form>
@endsection    
    
